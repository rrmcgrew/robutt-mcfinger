import os
import discord
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()

LEAGUE_BASE_URL = 'https://members.iracing.com/membersite/member/LeagueView.do?league='

token = os.getenv('BOT_SECRET')
server = os.getenv('SERVER')
mustang = os.getenv('MUSTANG_ID')
mazda = os.getenv('MAZDA_ID')
bot = commands.Bot(command_prefix='!')

def gen_league_response(car_label='car', league_id='0'):
    return f'Come join the fun in the {car_label}! {LEAGUE_BASE_URL}{league_id}'

@bot.event
async def on_ready():
    print(f'{bot.user} has connected to Discord!')
    guild = discord.utils.get(bot.guilds, name=server)
    print(f'{bot.user} is connected to: {guild.name}')

@bot.command(name='mustang')
async def mustang_league(ctx):
    if ctx.message.channel.name == 'leagues':
        response = gen_league_response(car_label='FR500S', league_id=mustang)
        await ctx.send(response)

@bot.command(name='mazda')
async def mazda_league(ctx):
    if ctx.message.channel.name == 'leagues':
        response = gen_league_response(car_label='MX5', league_id=mazda)
        await ctx.send(response)

@bot.command(name='leagues')
async def leagues(ctx, command, *args):
    session_info_help_text = 'Please state which league you would like session info for: mustang, mazda (i.e. `!leagues session-info mustang`)'

    if ctx.message.channel.name == 'admin-testing':
        response = None
        if command == 'help':
            if not args:
                response = 'Available commands: list, session-info'
            elif args[0] == 'session-info':
                response = session_info_help_text
            elif args[0] == 'list':
                response = 'Lists all available commands.'
            else:
                response = 'That command either doesn\'t exist.'
        elif command == 'list':
            response = 'We currently have the following leagues: Tuesday Night Mustang\'s, Sunday Night Mazda\'s'
        elif command == 'session-info':
            if not args:
                response = session_info_help_text
            elif args[0].lower() == 'mazda':
                response = 'Sunday - 20min practice @ TIME Eastern, 60 min open quali @ TIME Eastern, 30min race @ TIME Eastern'
            elif args[0].lower() == 'mustang':
                response = 'Tuesday - 20min practice @ TIME Eastern, 60 min open quali @ TIME Eastern, 30min race @ TIME Eastern'
            else:
                response = 'We only have the following leagues currently: mazda, mustang'
        
        if response:
            await ctx.send(response)

bot.run(token)
